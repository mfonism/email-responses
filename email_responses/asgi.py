"""
ASGI config for email_responses project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os

import dotenv
from django.core.asgi import get_asgi_application

from .settings import BASE_DIR

dotenv.load_dotenv(BASE_DIR / ".env")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "email_responses.settings")

application = get_asgi_application()
