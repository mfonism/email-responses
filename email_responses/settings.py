"""
Django settings for email_responses project.

Generated by 'django-admin startproject' using Django 3.2.7.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""

import os
from distutils.util import strtobool
from pathlib import Path

ALLOWED_HOSTS = []

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]
AUTH_USER_MODEL = "accounts.User"
AUTHENTICATION_BACKENDS = [
    "sesame.backends.ModelBackend",
    "django.contrib.auth.backends.ModelBackend",
]


BASE_DIR = Path(__file__).resolve().parent.parent


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}

DEBUG = strtobool(os.environ.get("DEBUG"))

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"


EMAIL_BACKEND = (
    "django.core.mail.backends.filebased.EmailBackend"
    if DEBUG
    else "sendgrid_backend.SendgridBackend"
)
EMAIL_FILE_PATH = BASE_DIR / "postoffice"


INSTALLED_APPS = [
    # custom auth app
    "accounts.apps.AccountsConfig",
    # default apps
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # sites framework
    "django.contrib.sites",
]


LANGUAGE_CODE = "en-us"

LOGIN_URL = "accounts:site_access_url_request"


MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "sesame.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    # to add current site to the request
    "django.contrib.sites.middleware.CurrentSiteMiddleware",
]


ROOT_URLCONF = "email_responses.urls"


SECRET_KEY = os.environ.get("SECRET_KEY")

SENDGRID_API_KEY = os.environ.get("SENDGRID_API_KEY")

SITE_ID = 1
SITE_NAME = "Email Responses"
SITE_DOMAIN = "https://emailresponses.io" if not DEBUG else "http://localhost:8000"

STATICFILES_DIRS = [BASE_DIR / "static"]
STATIC_ROOT = "static_root"
STATIC_URL = "/static/"


TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR / "templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

TIME_ZONE = "UTC"


USE_I18N = True
USE_L10N = True
USE_TZ = True


WSGI_APPLICATION = "email_responses.wsgi.application"


# max ages (in seconds)


def from_minutes(minutes):
    return minutes * 60


def from_hours(hours):
    return from_minutes(hours * 60)


REVOCATION_TOKEN_MAX_AGE = from_hours(12)
SESAME_MAX_AGE = from_minutes(8)
