from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model, logout, update_session_auth_hash
from django.forms.models import ModelForm

from . import exceptions, message_codes, services, tokens, utils

UserModel = get_user_model()


class AccountCreationForm(forms.ModelForm):
    class Meta:
        fields = ["name", "email"]
        model = UserModel

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        created_user = super().save(commit=commit)
        services.record_invite(inviter=self.request.user, invitee=created_user)
        services.send_site_access_url(recipient=created_user, first_time_user=True)
        messages.success(self.request, message_codes.ACCOUNT_CREATE_SUCCEEDED)
        return created_user


class AccountUpdateForm(forms.ModelForm):
    class Meta:
        fields = ["name"]
        model = UserModel


class AdminAccountUpdateForm(ModelForm):
    class Meta:
        model = UserModel
        fields = [
            "name",
            "is_active",
            "is_staff",
            "is_superuser",
            "groups",
            "user_permissions",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        user_permissions = self.fields.get("user_permissions")
        if user_permissions:
            user_permissions.queryset = user_permissions.queryset.select_related(
                "content_type"
            )


class LoginForm(forms.Form):
    email = forms.EmailField(help_text="Email address")

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super().__init__(*args, **kwargs)

    def send_site_access_url(self):
        email = self.cleaned_data["email"]
        try:
            user = UserModel.objects.get(email=email)
        except UserModel.DoesNotExist:
            user = None

        if user is not None:
            services.send_site_access_url(
                recipient=user, next_url=self.request.GET.get("next")
            )

        messages.success(self.request, message_codes.SITE_ACCESS_URL_REQUEST_SUCCEEDED)


class LogoutForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super().__init__(*args, **kwargs)

    def logout(self):
        logout(self.request)
        messages.success(self.request, message_codes.LOGOUT_SUCCEEDED)


class TokenRevocationURLRequestForm(forms.Form):
    email = forms.EmailField(help_text="Email address")

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super().__init__(*args, **kwargs)

    def send_token_revocation_url(self):
        email = self.cleaned_data["email"]
        try:
            user = UserModel.objects.get(email=email)
        except UserModel.DoesNotExist:
            user = None

        if user is not None:
            services.send_token_revocation_url(recipient=user)

        messages.success(self.request, message_codes.TOKEN_REVOKE_URL_REQUEST_SUCCEEDED)


class TokenRevocationForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        self.revocation_token = kwargs.pop("revocation_token")
        super().__init__(*args, **kwargs)

    def revoke_token(self):
        token_owner = tokens.get_token_owner(
            token=self.revocation_token,
            token_generator=tokens.token_revocation_token_generator,
            max_age=settings.REVOCATION_TOKEN_MAX_AGE,
        )
        if token_owner is None:
            messages.error(self.request, message_codes.TOKEN_REVOCATION_FAILED)
            raise exceptions.BadTokenException

        # evaluate and cache auth user
        # for updating session hash after password change
        auth_user = self.request.user or None
        token_owner.set_password(utils.generate_random_password())
        token_owner.password_last_set_at = utils.get_utc_now()
        token_owner.save()

        if token_owner == auth_user:
            update_session_auth_hash(self.request, token_owner)

        messages.success(self.request, message_codes.TOKEN_REVOCATION_SUCCEEDED)
