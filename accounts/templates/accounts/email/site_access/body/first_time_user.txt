{% load i18n %}{% blocktranslate with site_name=site.name %}You've just been invited to {{ site_name }} by the admin.

To accept this invite (and be automatically logged in to {{ site_name }}) please click on the link below:{% endblocktranslate %}

{{ site.domain }}{% url 'accounts:home' %}{{ sesame_qs }}

{% translate 'If you do not wish to accept the invite please ignore this mail and no actions will be taken on our end.' %}