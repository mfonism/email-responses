import uuid

from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from . import utils
from .managers import UserManager


class Invite(models.Model):
    created_at = models.DateTimeField(_("creation time"), default=utils.get_utc_now)
    recipient = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    sender = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        related_name="invites_from_me",
    )

    def __str__(self):
        return f"{self.sender} invites {self.recipient}"


class User(AbstractBaseUser, PermissionsMixin):
    uuid = models.UUIDField(
        _("UUID"), default=uuid.uuid4, unique=True, primary_key=True, editable=False
    )
    created_at = models.DateTimeField(_("creation time"), default=utils.get_utc_now)
    email = models.EmailField(_("email address"), unique=True)
    name = models.CharField(_("name"), max_length=64)
    is_active = models.BooleanField(_("active"), default=True)
    is_staff = models.BooleanField(_("staff status"), default=False)
    password_last_set_at = models.DateTimeField(
        _("password last set at"), default=utils.get_utc_now
    )

    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["name"]

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")
        swappable = "AUTH_USER_MODEL"

    def __str__(self):
        return f"<{self.email}>"

    def get_absolute_url(self):
        return reverse("accounts:account_read_public", kwargs={"pk": self.pk})
