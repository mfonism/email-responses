from django.conf import settings
from django.contrib.auth import get_user_model
from itsdangerous import URLSafeTimedSerializer
from itsdangerous.exc import BadData

UserModel = get_user_model()

# token for revoking all tokens owned by a particular user
TOKEN_REVOCATION_SALT = "token revocation salt"
token_revocation_token_generator = URLSafeTimedSerializer(
    secret_key=settings.SECRET_KEY, salt=TOKEN_REVOCATION_SALT
)


def get_token_owner(token, token_generator, max_age=None):
    try:
        deserialised = token_generator.loads(token, max_age=max_age)
    except BadData:
        return

    try:
        return UserModel.objects.get(**deserialised["kwargs"])
    except UserModel.DoesNotExist:
        return


def generate_token_revocation_token(user):
    return token_revocation_token_generator.dumps(
        {
            "kwargs": {
                "email": user.email,
                "password_last_set_at": str(user.password_last_set_at),
            }
        }
    )
