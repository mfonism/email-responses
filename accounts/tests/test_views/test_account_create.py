from django.urls import reverse
from pytest_django.asserts import assertRedirects, assertTemplateUsed  # noqa: E0401

from accounts.message_codes import ACCOUNT_CREATE_SUCCEEDED

from .utils import get_messages


def test_admin_can_GET_account_creation_page(client, admin):
    url = reverse("accounts:account_create")

    client.force_login(admin)
    resp = client.get(url)

    assert resp.status_code == 200
    assertTemplateUsed(resp, "accounts/account_create.html")
    assertTemplateUsed(resp, "base.html")


def test_admin_can_create_account(client, admin, django_user_model, mocker):
    url = reverse("accounts:account_create")
    payload = {"name": "Mfon Eti-mfon", "email": "mfonetimfon@gmail.com"}
    old_account_count = django_user_model.objects.count()
    # mocks for services
    mock_record_invite = mocker.patch("accounts.services.record_invite")
    mock_send_site_access_url = mocker.patch("accounts.services.send_site_access_url")

    client.force_login(admin)
    resp = client.post(url, payload)

    assertRedirects(resp, reverse("accounts:account_create_done"))
    assert django_user_model.objects.count() == old_account_count + 1
    created_user = django_user_model.objects.latest("created_at")
    assert created_user.name == "Mfon Eti-mfon"
    assert created_user.email == "mfonetimfon@gmail.com"
    # assert services are called
    mock_record_invite.assert_called_once_with(inviter=admin, invitee=created_user)
    mock_send_site_access_url.assert_called_once_with(
        recipient=created_user, first_time_user=True
    )
    # success message
    assert ACCOUNT_CREATE_SUCCEEDED in get_messages(resp.wsgi_request)


def test_non_admin_user_is_denied_permission_to_GET_page(client, non_admin_user):
    url = reverse("accounts:account_create")

    client.force_login(non_admin_user)
    resp = client.get(url)

    assert resp.status_code == 403


def test_non_admin_user_is_denied_permission_to_create_account(
    client, non_admin_user, django_user_model
):
    url = reverse("accounts:account_create")
    payload = {"name": "Mfon Eti-mfon", "email": "mfonetimfon@gmail.com"}
    old_account_count = django_user_model.objects.count()

    client.force_login(non_admin_user)
    resp = client.post(url, payload)

    assert resp.status_code == 403
    assert django_user_model.objects.count() == old_account_count


def test_GET_redirects_anonymous_user_to_login(client):
    url = reverse("accounts:account_create")

    resp = client.get(url)

    expected_redirect_url = reverse("accounts:site_access_url_request") + "?next=" + url
    assertRedirects(resp, expected_redirect_url)


def test_anonymous_user_is_redirected_to_login(client, django_user_model):
    url = reverse("accounts:account_create")
    payload = {"name": "Mfon Eti-mfon", "email": "mfonetimfon@gmail.com"}
    old_account_count = django_user_model.objects.count()

    resp = client.post(url, payload)

    expected_redirect_url = reverse("accounts:site_access_url_request") + "?next=" + url
    assertRedirects(resp, expected_redirect_url)
    assert django_user_model.objects.count() == old_account_count
