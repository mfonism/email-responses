from django.urls import reverse
from pytest_django.asserts import assertTemplateUsed  # noqa: E0401


def test_GET_page(client):
    url = reverse("accounts:site_access_url_request_done")

    resp = client.get(url)

    assert resp.status_code == 200
    assertTemplateUsed(resp, "accounts/site_access_url_request_done.html")
    assertTemplateUsed(resp, "base.html")
