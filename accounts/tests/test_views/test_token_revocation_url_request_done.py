from django.urls import reverse
from pytest_django.asserts import assertTemplateUsed  # noqa: E0401


def test_GET_page(client):
    url = reverse("accounts:token_revocation_url_request_done")

    resp = client.get(url)

    assert resp.status_code == 200
    assertTemplateUsed(resp, "accounts/token_revocation_url_request_done.html")
