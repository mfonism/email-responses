import pytest
from django.urls import reverse
from pytest_django.asserts import assertRedirects, assertTemplateUsed  # noqa: E0401

from accounts.message_codes import SITE_ACCESS_URL_REQUEST_SUCCEEDED

from .utils import get_messages


def test_GET_site_access_url_request_page(client):
    url = reverse("accounts:site_access_url_request")

    resp = client.get(url)

    assert resp.status_code == 200
    assertTemplateUsed(resp, "accounts/site_access_url_request.html")
    assertTemplateUsed(resp, "base.html")


def test_site_access_url_request(client, non_admin_user, mocker):
    url = reverse("accounts:site_access_url_request")
    payload = {"email": non_admin_user.email}
    mock_send_site_access_url = mocker.patch("accounts.services.send_site_access_url")

    resp = client.post(url, payload, follow=True)

    assertRedirects(resp, reverse("accounts:site_access_url_request_done"))
    logged_in_user = resp.context["request"].user
    assert logged_in_user.is_authenticated is False
    mock_send_site_access_url.assert_called_once_with(
        recipient=non_admin_user, next_url=None
    )
    # success message
    assert SITE_ACCESS_URL_REQUEST_SUCCEEDED in get_messages(resp.wsgi_request)


@pytest.mark.django_db
def test_site_access_url_is_not_sent_to_unrecognised_email(client, mocker):
    url = reverse("accounts:site_access_url_request")
    payload = {"email": "mfonetimfon@gmail.com"}
    mock_send_site_access_url = mocker.patch("accounts.services.send_site_access_url")

    resp = client.post(url, payload, follow=True)

    assertRedirects(resp, reverse("accounts:site_access_url_request_done"))
    mock_send_site_access_url.assert_not_called()
    # success message is sent regardless
    assert SITE_ACCESS_URL_REQUEST_SUCCEEDED in get_messages(resp.wsgi_request)


def test_next_parameter_is_forwarded_to_site_access_url_sending_service(
    client, non_admin_user, mocker
):
    url = reverse("accounts:site_access_url_request") + "?next=/mails/1/responses/"
    payload = {"email": non_admin_user.email}
    mock_send_site_access_url = mocker.patch("accounts.services.send_site_access_url")

    resp = client.post(url, payload, follow=True)

    assertRedirects(resp, reverse("accounts:site_access_url_request_done"))
    mock_send_site_access_url.assert_called_once_with(
        recipient=non_admin_user, next_url="/mails/1/responses/"
    )
    # success message
    assert SITE_ACCESS_URL_REQUEST_SUCCEEDED in get_messages(resp.wsgi_request)
