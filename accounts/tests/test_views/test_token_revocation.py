from django.urls import reverse
from pytest_django.asserts import assertRedirects, assertTemplateUsed  # noqa: E0401

from accounts.message_codes import TOKEN_REVOCATION_FAILED, TOKEN_REVOCATION_SUCCEEDED
from accounts.tokens import token_revocation_token_generator

from .utils import get_messages


def test_GET_token_revocation_page(client):
    url = reverse(
        "accounts:token_revocation", kwargs={"revocation_token": "a+revocation+token"}
    )

    resp = client.get(url)

    assert resp.status_code == 200
    assertTemplateUsed(resp, "accounts/token_revocation.html")


def test_revoke_token(client, django_user_model, mocker, now):
    mfon = django_user_model.objects.create_user(
        name="Mfon Eti-mfon", email="mfonetimfon@gmail.com", password="old password"
    )
    url = reverse(
        "accounts:token_revocation", kwargs={"revocation_token": "a+revocation+token"}
    )
    # util mocks
    mock_get_token_owner = mocker.patch(
        "accounts.tokens.get_token_owner", return_value=mfon
    )
    mock_generate_random_password = mocker.patch(
        "accounts.utils.generate_random_password", return_value="new password"
    )
    mock_get_utc_now = mocker.patch("accounts.utils.get_utc_now", return_value=now)

    client.force_login(mfon)
    resp = client.post(url, {}, follow=True)

    assertRedirects(resp, reverse("accounts:home"))
    mock_get_token_owner.assert_called_once_with(
        token="a+revocation+token",
        token_generator=token_revocation_token_generator,
        max_age=12 * 60 * 60,
    )
    mock_generate_random_password.assert_called_once()
    mock_get_utc_now.assert_called_once()
    mfon.refresh_from_db()
    assert resp.context["request"].user == mfon
    assert mfon.password_last_set_at == now
    assert mfon.check_password("new password")
    # success message
    assert TOKEN_REVOCATION_SUCCEEDED in get_messages(resp.wsgi_request)


def test_revoke_token_failed(client, django_user_model, mocker):
    mfon = django_user_model.objects.create_user(
        name="Mfon Eti-mfon", email="mfonetimfon@gmail.com", password="old password"
    )
    last_password_set_time = mfon.password_last_set_at
    url = reverse(
        "accounts:token_revocation", kwargs={"revocation_token": "a+bad+token"}
    )
    mock_get_token_owner = mocker.patch(
        "accounts.tokens.get_token_owner", return_value=None
    )

    client.force_login(mfon)
    resp = client.post(url, {}, follow=True)

    assertRedirects(resp, reverse("accounts:token_revocation_failed"))
    mock_get_token_owner.assert_called_once_with(
        token="a+bad+token",
        token_generator=token_revocation_token_generator,
        max_age=12 * 60 * 60,
    )
    mfon.refresh_from_db()
    assert resp.context["request"].user == mfon
    assert mfon.password_last_set_at == last_password_set_time
    assert mfon.check_password("old password")
    # failure message
    assert TOKEN_REVOCATION_FAILED in get_messages(resp.wsgi_request)


def test_user_can_revoke_token_for_someone_else(client, django_user_model, mocker, now):
    mfon = django_user_model.objects.create_user(
        name="Mfon Eti-mfon",
        email="mfonetimfon@gmail.com",
        password="mfon's old password",
    )
    someone_else = django_user_model.objects.create_user(
        name="someone else",
        email="someone@else.com",
        password="someone else's old password",
    )
    last_password_set_times = {
        "mfon": mfon.password_last_set_at,
        "someone_else": someone_else.password_last_set_at,
    }

    url = reverse(
        "accounts:token_revocation", kwargs={"revocation_token": "a+revocation+token"}
    )
    mock_get_token_owner = mocker.patch(
        "accounts.tokens.get_token_owner", return_value=someone_else
    )
    mock_generate_random_password = mocker.patch(
        "accounts.utils.generate_random_password", return_value="new password"
    )
    mock_get_utc_now = mocker.patch("accounts.utils.get_utc_now", return_value=now)

    client.force_login(mfon)
    resp = client.post(url, {}, follow=True)

    assertRedirects(resp, reverse("accounts:home"))
    mock_get_token_owner.assert_called_once_with(
        token="a+revocation+token",
        token_generator=token_revocation_token_generator,
        max_age=12 * 60 * 60,
    )
    mock_generate_random_password.assert_called_once()
    mock_get_utc_now.assert_called_once()
    # mfon stays logged in and maintaining their old state
    mfon.refresh_from_db()
    assert resp.context["request"].user == mfon
    assert mfon.password_last_set_at == last_password_set_times["mfon"]
    assert mfon.check_password("mfon's old password")
    # state of token owner (someone else) is changed appropriately
    someone_else.refresh_from_db()
    assert someone_else.password_last_set_at == now
    assert someone_else.check_password("new password")
    # success message
    assert TOKEN_REVOCATION_SUCCEEDED in get_messages(resp.wsgi_request)


def test_anonymous_user_can_revoke_token(client, django_user_model, mocker, now):
    mfon = django_user_model.objects.create_user(
        name="Mfon Eti-mfon", email="mfonetimfon@gmail.com", password="old password"
    )
    url = reverse(
        "accounts:token_revocation", kwargs={"revocation_token": "a+revocation+token"}
    )
    # util mocks
    mock_get_token_owner = mocker.patch(
        "accounts.tokens.get_token_owner", return_value=mfon
    )
    mock_generate_random_password = mocker.patch(
        "accounts.utils.generate_random_password", return_value="new password"
    )
    mock_get_utc_now = mocker.patch("accounts.utils.get_utc_now", return_value=now)

    resp = client.post(url, {}, follow=True)

    assertRedirects(resp, reverse("accounts:home"))
    mock_get_token_owner.assert_called_once_with(
        token="a+revocation+token",
        token_generator=token_revocation_token_generator,
        max_age=12 * 60 * 60,
    )
    mock_generate_random_password.assert_called_once()
    mock_get_utc_now.assert_called_once()
    mfon.refresh_from_db()
    assert resp.context["request"].user.is_authenticated is False
    assert mfon.password_last_set_at == now
    assert mfon.check_password("new password")
    # success message
    assert TOKEN_REVOCATION_SUCCEEDED in get_messages(resp.wsgi_request)
