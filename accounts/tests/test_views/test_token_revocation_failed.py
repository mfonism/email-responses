from django.urls import reverse
from pytest_django.asserts import assertTemplateUsed  # noqa: E0401


def test_GET_token_revoke_url_request_page(client):
    url = reverse("accounts:token_revocation_failed")

    resp = client.get(url)

    assert resp.status_code == 200
    assertTemplateUsed(resp, "accounts/token_revocation_failed.html")
