import pytest
from django.urls import reverse
from pytest_django.asserts import assertRedirects, assertTemplateUsed  # noqa: E0401

from accounts.message_codes import TOKEN_REVOKE_URL_REQUEST_SUCCEEDED

from .utils import get_messages


def test_GET_token_revoke_url_request_page(client):
    url = reverse("accounts:token_revocation_url_request")

    resp = client.get(url)

    assert resp.status_code == 200
    assertTemplateUsed(resp, "accounts/token_revocation_url_request.html")


def test_request_token_revocation_url(client, non_admin_user, mocker):
    url = reverse("accounts:token_revocation_url_request")
    payload = {"email": non_admin_user.email}
    mock_send_token_revocation_url = mocker.patch(
        "accounts.services.send_token_revocation_url"
    )

    resp = client.post(url, payload, follow=True)

    expected_url = reverse("accounts:token_revocation_url_request_done")
    assertRedirects(resp, expected_url)
    mock_send_token_revocation_url.assert_called_once_with(recipient=non_admin_user)
    # success message
    assert TOKEN_REVOKE_URL_REQUEST_SUCCEEDED in get_messages(resp.wsgi_request)


@pytest.mark.django_db
def test_token_revocation_url_is_not_sent_to_unrecognised_email(client, mocker):
    url = reverse("accounts:token_revocation_url_request")
    payload = {"email": "mfonetimfon@gmail.com"}
    mock_send_token_revocation_url = mocker.patch(
        "accounts.services.send_token_revocation_url"
    )

    resp = client.post(url, payload, follow=True)

    expected_url = reverse("accounts:token_revocation_url_request_done")
    assertRedirects(resp, expected_url)
    mock_send_token_revocation_url.assert_not_called()
    # success message is sent regardless
    assert TOKEN_REVOKE_URL_REQUEST_SUCCEEDED in get_messages(resp.wsgi_request)
