from django.contrib import messages


def get_messages(request, tag=None):
    return [
        str(message)
        for message in messages.get_messages(request)
        if tag is None or tag in message.tags.split()
    ]
