import pytest
from django.urls import reverse
from pytest_django.asserts import assertRedirects, assertTemplateUsed  # noqa: E0401

from accounts.message_codes import LOGOUT_SUCCEEDED

from .utils import get_messages


def test_GET_logout_page(client):
    url = reverse("accounts:logout")

    resp = client.get(url)

    assert resp.status_code == 200
    assertTemplateUsed(resp, "accounts/logout.html")


def test_logout(client, non_admin_user):
    url = reverse("accounts:logout")

    client.force_login(non_admin_user)
    resp = client.post(url, {})

    assertRedirects(resp, reverse("accounts:site_access_url_request"))
    user = resp.wsgi_request.user
    assert user.is_authenticated is False
    # success message
    assert LOGOUT_SUCCEEDED in get_messages(resp.wsgi_request)


@pytest.mark.django_db
def test_anonymous_user_can_logout(client):
    # logout should not give any clue as to whether
    # a user is logged in or not
    url = reverse("accounts:logout")

    resp = client.post(url, {})

    assertRedirects(resp, reverse("accounts:site_access_url_request"))
    user = resp.wsgi_request.user
    assert user.is_authenticated is False
    # success message
    assert LOGOUT_SUCCEEDED in get_messages(resp.wsgi_request)
