from django.urls import reverse
from pytest_django.asserts import assertRedirects, assertTemplateUsed  # noqa: E0401


def test_user_can_GET_update_page_for_their_account(client, non_admin_user):
    url = reverse("accounts:account_update")

    client.force_login(non_admin_user)
    resp = client.get(url)

    assert resp.status_code == 200
    assertTemplateUsed(resp, "accounts/account_update.html")


def test_user_can_update_their_account(client, non_admin_user):
    url = reverse("accounts:account_update")

    client.force_login(non_admin_user)
    resp = client.post(url, {"name": "an updated name"})

    expected_url = reverse("accounts:account_read_private")
    assertRedirects(resp, expected_url)
    non_admin_user.refresh_from_db()
    assert non_admin_user.name == "an updated name"


def test_GET_redirects_anonymous_user_to_login(client):
    url = reverse("accounts:account_update")

    resp = client.get(url)

    expected_url = reverse("accounts:site_access_url_request") + "?next=" + url
    assertRedirects(resp, expected_url)


def test_POST_redirects_anonymous_user_to_login(client):
    url = reverse("accounts:account_update")

    resp = client.post(url, {"name": "an updated name"})

    expected_url = reverse("accounts:site_access_url_request") + "?next=" + url
    assertRedirects(resp, expected_url)
