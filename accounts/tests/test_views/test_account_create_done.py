from django.urls import reverse
from pytest_django.asserts import assertRedirects, assertTemplateUsed  # noqa: E0401


def test_admin_can_GET_page(client, admin):
    url = reverse("accounts:account_create_done")

    client.force_login(admin)
    resp = client.get(url)

    assert resp.status_code == 200
    assertTemplateUsed(resp, "accounts/account_create_done.html")
    assertTemplateUsed(resp, "base.html")


def test_non_admin_user_is_denied_permission_to_GET_page(client, non_admin_user):
    url = reverse("accounts:account_create_done")

    client.force_login(non_admin_user)
    resp = client.get(url)

    assert resp.status_code == 403


def test_GET_redirects_anonymous_user_to_login(client):
    url = reverse("accounts:account_create_done")

    resp = client.get(url)

    expected_redirect_url = reverse("accounts:site_access_url_request") + "?next=" + url
    assertRedirects(resp, expected_redirect_url)
