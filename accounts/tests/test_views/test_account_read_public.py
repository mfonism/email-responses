from django.urls import reverse
from pytest_django.asserts import assertTemplateUsed  # noqa: E0401


def test_GET_page(client, django_user_model):
    mfon = django_user_model.objects.create_user(
        name="Mfon Eti-mfon", email="mfonetimfon@gmail.com"
    )
    url = reverse("accounts:account_read_public", kwargs={"pk": mfon.pk})

    resp = client.get(url)

    assert resp.status_code == 200
    assertTemplateUsed(resp, "accounts/account_read_public.html")
    assert resp.context["user"] == mfon
