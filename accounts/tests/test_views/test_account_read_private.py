from django.urls import reverse
from pytest_django.asserts import assertRedirects, assertTemplateUsed  # noqa: E0401


def test_user_can_GET_page_for_their_account(client, non_admin_user):
    url = reverse("accounts:account_read_private")

    client.force_login(non_admin_user)
    resp = client.get(url)

    assert resp.status_code == 200
    assertTemplateUsed(resp, "accounts/account_read_private.html")


def test_anonymous_user_is_redirected_to_login(client):
    url = reverse("accounts:account_read_private")

    resp = client.get(url)

    expected_url = reverse("accounts:site_access_url_request") + "?next=" + url
    assertRedirects(resp, expected_url)
