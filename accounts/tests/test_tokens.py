from datetime import timedelta

import pytest
from itsdangerous.exc import (
    BadData,
    BadHeader,
    BadPayload,
    BadSignature,
    BadTimeSignature,
)

from accounts import tokens


@pytest.mark.parametrize(
    "error_class", [BadData, BadHeader, BadPayload, BadSignature, BadTimeSignature]
)
def test_get_token_owner_returns_no_owner_for_bad_token(error_class):
    class DummyTokenGenerator:
        def loads(self, token, **kwargs):
            raise error_class("this token has problems, hun!")

    assert tokens.get_token_owner("a+bad+token", DummyTokenGenerator()) is None


def test_revocation_token_owner(non_admin_user):
    revocation_token = tokens.generate_token_revocation_token(non_admin_user)

    assert (
        tokens.get_token_owner(
            revocation_token, tokens.token_revocation_token_generator
        )
        == non_admin_user
    )


def test_revocation_token_is_detached_from_owner_after_first_use(non_admin_user):
    revocation_token = tokens.generate_token_revocation_token(non_admin_user)
    # after first use of revocation token the user's `password_last_set_at`
    # field should be updated so that it is detached
    non_admin_user.password_last_set_at += timedelta(seconds=1)
    non_admin_user.save()

    assert (
        tokens.get_token_owner(
            revocation_token, tokens.token_revocation_token_generator
        )
        is None
    )
