from uuid import UUID

import pytest

from accounts.models import Invite, User


def test_str_invite(admin, non_admin_user):
    invite = Invite.objects.create(sender=admin, recipient=non_admin_user)

    assert str(invite) == f"{admin} invites {non_admin_user}"


@pytest.mark.django_db
def test_create_user():
    mfon = User.objects.create_user(
        name="Mfon Eti-mfon", email="mfonetimfon@gmail.com", password="weak password"
    )

    assert isinstance(mfon.pk, UUID)
    assert mfon.name == "Mfon Eti-mfon"
    assert mfon.email == "mfonetimfon@gmail.com"
    assert mfon.check_password("weak password")
    assert mfon.is_active is True
    assert mfon.is_staff is False
    assert mfon.is_superuser is False


@pytest.mark.django_db
def test_create_superuser():
    mfon = User.objects.create_superuser(
        name="Mfon Eti-mfon", email="mfonetimfon@gmail.com", password="weak password"
    )

    assert isinstance(mfon.pk, UUID)
    assert mfon.name == "Mfon Eti-mfon"
    assert mfon.email == "mfonetimfon@gmail.com"
    assert mfon.check_password("weak password")
    assert mfon.is_active is True
    assert mfon.is_staff is True
    assert mfon.is_superuser is True


@pytest.mark.django_db
def test_str_user():
    mfon = User.objects.create_user(
        name="Mfon Eti-mfon", email="mfonetimfon@gmail.com", password="weak password"
    )

    assert str(mfon) == "<mfonetimfon@gmail.com>"
