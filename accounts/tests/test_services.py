from django.urls import reverse
from django.utils.translation import gettext as _

from accounts.models import Invite
from accounts.services import (
    record_invite,
    send_site_access_url,
    send_token_revocation_url,
)


def test_record_invite(admin, non_admin_user):
    old_invite_count = Invite.objects.count()

    record_invite(inviter=admin, invitee=non_admin_user)

    assert Invite.objects.count() == old_invite_count + 1
    created_invite = Invite.objects.latest("created_at")
    assert created_invite.sender == admin
    assert created_invite.recipient == non_admin_user


def test_send_site_access_url(non_admin_user, mocker):
    mock_email_message = mocker.patch("accounts.services.EmailMessage")
    mock_get_sesame_qs = mocker.patch(
        "accounts.services.get_query_string", return_value="?sesame=open"
    )

    send_site_access_url(non_admin_user)

    mock_get_sesame_qs.assert_called_once_with(non_admin_user)
    mock_email_message.assert_called_once()
    call_kwargs = mock_email_message.call_args.kwargs
    # subject of mail
    assert call_kwargs["subject"].startswith(_("Your login access to"))
    # body of mail
    assert non_admin_user.name in call_kwargs["body"]
    assert _("invited to") not in call_kwargs["body"]
    assert f"{reverse('accounts:home')}?sesame=open" in call_kwargs["body"]
    # recipient
    assert call_kwargs["to"] == [non_admin_user.email]
    # mail sending
    mock_email_message.return_value.send.assert_called_once()


def test_send_site_access_url_to_first_time_user(non_admin_user, mocker):
    mock_email_message = mocker.patch("accounts.services.EmailMessage")
    mock_get_sesame_qs = mocker.patch(
        "accounts.services.get_query_string", return_value="?sesame=open"
    )

    send_site_access_url(non_admin_user, first_time_user=True)

    mock_get_sesame_qs.assert_called_once_with(non_admin_user)
    mock_email_message.assert_called_once()
    call_kwargs = mock_email_message.call_args.kwargs
    # subject of mail
    assert call_kwargs["subject"].startswith(_("Your invitation to"))
    # body of mail
    assert non_admin_user.name in call_kwargs["body"]
    assert _("invited to") in call_kwargs["body"]
    assert f"{reverse('accounts:home')}?sesame=open" in call_kwargs["body"]
    # recipient
    assert call_kwargs["to"] == [non_admin_user.email]
    # mail sending
    mock_email_message.return_value.send.assert_called_once()


def test_send_site_access_url_with_next_url(non_admin_user, mocker):
    mock_email_message = mocker.patch("accounts.services.EmailMessage")
    mock_get_sesame_qs = mocker.patch(
        "accounts.services.get_query_string", return_value="?sesame=open"
    )

    send_site_access_url(non_admin_user, next_url="/mails/1/responses/")

    mock_get_sesame_qs.assert_called_once_with(non_admin_user)
    mock_email_message.assert_called_once()
    call_kwargs = mock_email_message.call_args.kwargs
    # subject of mail
    assert call_kwargs["subject"].startswith(_("Your login access to"))
    # body of mail
    assert non_admin_user.name in call_kwargs["body"]
    assert _("invited to") not in call_kwargs["body"]
    assert "/mails/1/responses/?sesame=open" in call_kwargs["body"]
    assert f"{reverse('accounts:home')}?sesame=open" in call_kwargs["body"]
    # recipient
    assert call_kwargs["to"] == [non_admin_user.email]
    # mail sending
    mock_email_message.return_value.send.assert_called_once()


def test_send_token_revocation_url(non_admin_user, mocker):
    mock_email_message = mocker.patch("accounts.services.EmailMessage")
    mock_generate_revocation_token = mocker.patch(
        "accounts.tokens.generate_token_revocation_token",
        return_value="revocation+token",
    )

    send_token_revocation_url(recipient=non_admin_user)

    mock_generate_revocation_token.assert_called_once_with(user=non_admin_user)
    mock_email_message.assert_called_once()
    email_call_kwargs = mock_email_message.call_args.kwargs
    # subject of mail
    assert email_call_kwargs["subject"].startswith(_("Revoke your tokens on"))
    # body of mail
    assert non_admin_user.name in email_call_kwargs["body"]
    revocation_url = reverse(
        "accounts:token_revocation", kwargs={"revocation_token": "revocation+token"}
    )
    assert revocation_url in email_call_kwargs["body"]
    assert email_call_kwargs["to"] == [non_admin_user.email]
    # mail sending
    mock_email_message.return_value.send.assert_called_once()
