from django.conf import settings

from accounts.tests.test_data_migrations import MigrationsTestCase


class TestDefaultSite(MigrationsTestCase):

    old_behavior_migration = "0005_user_created_at"
    new_behavior_migration = "0006_customize_default_site"

    def setUpWithOldBehavior(self, apps):
        SiteModel = apps.get_model("sites", "Site")
        default_site = SiteModel.objects.get(pk=getattr(settings, "SITE_ID", 1))

        self.assertEqual(default_site.name, "example.com")
        self.assertEqual(default_site.domain, "example.com")

    def test_default_site(self):
        SiteModel = self.apps.get_model("sites", "Site")
        default_site = SiteModel.objects.get(pk=getattr(settings, "SITE_ID", 1))

        self.assertEqual(default_site.name, settings.SITE_NAME)
        self.assertEqual(default_site.domain, settings.SITE_DOMAIN)
