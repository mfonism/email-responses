import pytest
from django.utils import timezone


@pytest.fixture
def non_admin_password():
    return "password"


@pytest.fixture
def admin(django_user_model):
    return django_user_model.objects.create_superuser(
        name="admin", email="admin@user.com"
    )


@pytest.fixture
def non_admin_user(django_user_model, non_admin_password):
    return django_user_model.objects.create_user(
        name="non admin user", email="non.admin@user.com", password=non_admin_password
    )


@pytest.fixture
def now():
    return timezone.now()
