from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import (
    CreateView,
    DetailView,
    FormView,
    TemplateView,
    UpdateView,
)

from .exceptions import BadTokenException
from .forms import (
    AccountCreationForm,
    AccountUpdateForm,
    LoginForm,
    LogoutForm,
    TokenRevocationForm,
    TokenRevocationURLRequestForm,
)

UserModel = get_user_model()


class AccountCreateView(CreateView):
    model = UserModel
    form_class = AccountCreationForm
    success_url = reverse_lazy("accounts:account_create_done")
    template_name = "accounts/account_create.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs


class AccountCreateDoneView(TemplateView):
    template_name = "accounts/account_create_done.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)


class AccountReadPrivateView(LoginRequiredMixin, TemplateView):
    queryset = UserModel.objects.select_related("invite__sender")
    template_name = "accounts/account_read_private.html"

    def get_user(self):
        try:
            obj = self.queryset.get(pk=self.request.user.pk)
        except self.queryset.model.DoesNotExist:
            raise Http404

        return obj

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data["user"] = context_data["object"] = self.get_user()
        return context_data


class AccountReadPublicView(DetailView):
    queryset = UserModel.objects.select_related("invite__sender")
    template_name = "accounts/account_read_public.html"


class AccountUpdateView(LoginRequiredMixin, UpdateView):
    form_class = AccountUpdateForm
    success_url = reverse_lazy("accounts:account_read_private")
    template_name = "accounts/account_update.html"

    def get_object(self, queryset=None):
        return self.request.user


class HomeView(TemplateView):
    template_name = "accounts/home.html"


class LogoutView(FormView):
    form_class = LogoutForm
    success_url = reverse_lazy("accounts:site_access_url_request")
    template_name = "accounts/logout.html"

    def form_valid(self, form):
        form.logout()
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs


class SiteAccessURLRequestView(FormView):
    form_class = LoginForm
    success_url = reverse_lazy("accounts:site_access_url_request_done")
    template_name = "accounts/site_access_url_request.html"

    def form_valid(self, form):
        form.send_site_access_url()
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs


class SiteAccessURLRequestDoneView(TemplateView):
    template_name = "accounts/site_access_url_request_done.html"


class TokenRevocationURLRequestView(FormView):
    form_class = TokenRevocationURLRequestForm
    success_url = reverse_lazy("accounts:token_revocation_url_request_done")
    template_name = "accounts/token_revocation_url_request.html"

    def form_valid(self, form):
        form.send_token_revocation_url()
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs


class TokenRevocationURLRequestDoneView(TemplateView):
    template_name = "accounts/token_revocation_url_request_done.html"


class TokenRevocationView(FormView):
    form_class = TokenRevocationForm
    success_url = reverse_lazy("accounts:home")
    template_name = "accounts/token_revocation.html"

    def form_valid(self, form):
        try:
            form.revoke_token()
        except BadTokenException:
            return HttpResponseRedirect(reverse("accounts:token_revocation_failed"))

        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["request"] = self.request
        kwargs["revocation_token"] = self.kwargs["revocation_token"]
        return kwargs


class TokenRevocationFailedView(TemplateView):
    template_name = "accounts/token_revocation_failed.html"
