class EmailResponsesBaseException(Exception):
    pass


class BadTokenException(EmailResponsesBaseException):
    pass
