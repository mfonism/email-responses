from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.translation import gettext_lazy as _
from sesame.utils import get_query_string

from . import tokens
from .models import Invite


def record_invite(inviter, invitee):
    Invite.objects.create(sender=inviter, recipient=invitee)


def send_site_access_url(recipient, next_url=None, first_time_user=False):
    current_site = Site.objects.get_current()
    sesame_qs = get_query_string(recipient)
    # strip next_url of all query params
    # to avoid having duplicate sesame params
    if next_url:
        next_url = next_url.split("?")[0]

    if first_time_user:
        subject = _("Your invitation to ") + current_site.name
    else:
        subject = _("Your login access to ") + current_site.name
    body = render_to_string(
        "accounts/email/site_access/body/index.txt",
        {
            "user": recipient,
            "site": current_site,
            "sesame_qs": sesame_qs,
            "is_first_time_user": first_time_user,
            "next_url": next_url,
        },
    )
    email = EmailMessage(subject=subject, body=body, to=[recipient.email])
    email.send()


def send_token_revocation_url(recipient):
    current_site = Site.objects.get_current()
    revocation_token = tokens.generate_token_revocation_token(user=recipient)

    subject = _("Revoke your tokens on ") + current_site.name
    body = render_to_string(
        "accounts/email/token_revocation/body/index.txt",
        {
            "user": recipient,
            "site": current_site,
            "revocation_token": revocation_token,
        },
    )
    email = EmailMessage(subject=subject, body=body, to=[recipient.email])
    email.send()
