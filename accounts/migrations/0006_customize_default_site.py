from django.conf import settings
from django.db import migrations


def customise_default_site(apps, schema_editor):
    SiteModel = apps.get_model("sites", "Site")
    db_alias = schema_editor.connection.alias
    SiteModel.objects.using(db_alias).update_or_create(
        pk=getattr(settings, "SITE_ID", 1),
        defaults={"name": settings.SITE_NAME, "domain": settings.SITE_DOMAIN},
    )


def reverse_default_site_customisation(apps, schema_editor):
    SiteModel = apps.get_model("sites", "Site")
    db_alias = schema_editor.connection.alias
    SiteModel.objects.using(db_alias).update_or_create(
        pk=getattr(settings, "SITE_ID", 1),
        defaults={"name": "example.com", "domain": "example.com"},
    )


class Migration(migrations.Migration):

    dependencies = [("accounts", "0005_user_created_at")]

    operations = [
        migrations.RunPython(
            customise_default_site, reverse_code=reverse_default_site_customisation
        )
    ]
