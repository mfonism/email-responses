# Generated by Django 3.2.7 on 2021-09-17 10:08

from django.db import migrations

import accounts.managers


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0001_initial"),
    ]

    operations = [
        migrations.AlterModelManagers(
            name="user",
            managers=[
                ("objects", accounts.managers.UserManager()),
            ],
        ),
    ]
