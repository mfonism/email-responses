# message codes
ACCOUNT_CREATE_SUCCEEDED = (
    "An invite has just been sent to the email address you provided."
)
LOGOUT_SUCCEEDED = "You've been successfully logged out."
SITE_ACCESS_URL_REQUEST_SUCCEEDED = (
    "A magic login link has just been sent to the email address you provided"
)
TOKEN_REVOKE_URL_REQUEST_SUCCEEDED = "A link to where you can confirm revocation of your tokens has just been sent to the email address you provided"
TOKEN_REVOCATION_FAILED = "Token revocation failed."
TOKEN_REVOCATION_SUCCEEDED = "You have successfully revoked your all your tokens."
