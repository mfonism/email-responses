from django.urls import path

from .views import (
    AccountCreateDoneView,
    AccountCreateView,
    AccountReadPrivateView,
    AccountReadPublicView,
    AccountUpdateView,
    HomeView,
    LogoutView,
    SiteAccessURLRequestDoneView,
    SiteAccessURLRequestView,
    TokenRevocationFailedView,
    TokenRevocationURLRequestDoneView,
    TokenRevocationURLRequestView,
    TokenRevocationView,
)

app_name = "accounts"
urlpatterns = [
    path("home/", HomeView.as_view(), name="home"),
    path("invite/", AccountCreateView.as_view(), name="account_create"),
    path("invite/done/", AccountCreateDoneView.as_view(), name="account_create_done"),
    path("login/", SiteAccessURLRequestView.as_view(), name="site_access_url_request"),
    path(
        "login/done/",
        SiteAccessURLRequestDoneView.as_view(),
        name="site_access_url_request_done",
    ),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("me/", AccountReadPrivateView.as_view(), name="account_read_private"),
    path("me/update/", AccountUpdateView.as_view(), name="account_update"),
    path(
        "token/revoke/",
        TokenRevocationURLRequestView.as_view(),
        name="token_revocation_url_request",
    ),
    path(
        "token/revoke/done/",
        TokenRevocationURLRequestDoneView.as_view(),
        name="token_revocation_url_request_done",
    ),
    path(
        "token/revoke/confirm/failed/",
        TokenRevocationFailedView.as_view(),
        name="token_revocation_failed",
    ),
    path(
        "token/revoke/<str:revocation_token>/confirm/",
        TokenRevocationView.as_view(),
        name="token_revocation",
    ),
    path(
        "user/<uuid:pk>/", AccountReadPublicView.as_view(), name="account_read_public"
    ),
]
