from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext_lazy as _

from .forms import AdminAccountUpdateForm
from accounts.models import Invite

UserModel = get_user_model()


@admin.register(Invite)
class InviteAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False


@admin.register(UserModel)
class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {"fields": ("name", "email")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (
            _("Important dates"),
            {
                "fields": (
                    "created_at",
                    "last_login",
                )
            },
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("name",),
            },
        ),
    )
    form = AdminAccountUpdateForm
    list_display = ("name", "email", "is_active", "is_staff", "created_at")
    search_fields = ("name", "email")
    ordering = ("name", "-created_at")

    def has_add_permission(self, request):
        return False
