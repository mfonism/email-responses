import secrets

from django.utils import timezone


def generate_random_password():
    return secrets.token_urlsafe(48)


def get_utc_now():
    return timezone.now()
